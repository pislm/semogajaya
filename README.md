# Semoga Jaya
[![pipeline status](https://gitlab.com/pislm/semogajaya/badges/master/pipeline.svg)](https://gitlab.com/pislm/semogajaya/-/commits/master) [![coverage report](https://gitlab.com/pislm/semogajaya/badges/master/coverage.svg)](https://gitlab.com/pislm/semogajaya/-/commits/master)

Repositori ini merupakan tugas akhir untuk mata kuliah Pengantar Pemrograman Web tahun 2021

Link HerokuApp : semogajaya.herokuapp.com

## Anggota Kelompok
- 1706984770 Tri Rahayu  
- 1906399026 Muhammad Nafis Hibatullah  
- 1906399171 Hafizh Salam
- 1906426815 Fauzan Hanandito  
- 1906440486 Ryan Yakhin Kogoya
